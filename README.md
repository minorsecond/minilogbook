# Minilogbook

[![Build Status](http://ci.imspatial.me/buildStatus/icon?job=miniLogbook/GUI_work)](http://ci.imspatial.me/job/miniLogbook/job/GUI_work/)

a no-frills, minimal logbook for Amateur Radio Operators. This project aims to provide a logbook with a sleek UI without all of the bloat of many other logbooks. 

## Getting Started

Clone the repository and install the following packages:

* sqlalchemy
* configobj
* PyQt4
* nose (for testing)
* pyinstaller

I install all of the packages on a conda environment for easy removal.

### Prerequisities

* Python 3.4
* Either Windows or WINE with python and the necessary packages
* pyinstaller (if you plan on packaging an exe)

## Running the tests

In the terminal, browse to the miniLogbook directory and type "nosetests." Nose should find the tests and run them automagically.

## Deployment

For packaging a Windows executable, simply run "pyinstaller.exe miniLogbook.spec." You'll end up with a "dist" directory containing a binary. 

## Authors

* **Robert Ross Wardrup*

## License

[See License Page](http://www.binpress.com/license/view/l/3a5a622007bd5cbd075273e6c13d2afd)