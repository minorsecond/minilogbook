#!/usr/bin/env bash

# Create .py from .ui for GUI
chmod +x ./build-gui.sh
./build-gui.sh

#Activate anaconda and build in WINE
. ~/anaconda3/bin/activate miniLogbook
. ~/DEV/wine_environments/venv_wine/bin/activate
wine c:/Python34/python.exe ~/DEV/pyinstaller/pyinstaller.py --clean ./miniLogbook.spec