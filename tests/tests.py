from unittest import TestCase
import os
import filecmp
from sqlalchemy import inspect, create_engine
from ml import *

test_path = os.path.dirname(os.path.abspath(__file__))

class TestIoFunctions(TestCase):
    def test_write_config(self):
        """Config file is being incorrectly written."""
        golden_file = join(test_path, 'golden_config.ini')
        test_file = join(test_path, 'test_config.ini')
        print(test_file)

        test_settings = {
            'user_callsign':    'KD5LPB',
            'user_grid':        'EM03',
            'default_rst_bool': True,
            'default_rst_value':    599,
            'default_power_bool':   True,
            'default_power_value':  100
        }

        write_config(test_file, test_settings)

        self.assertTrue(filecmp.cmp(golden_file, test_file))

    def test_read_config(self):
        """Config file not being read correctly."""
        test_file = join(test_path, 'golden_config.ini')

        expected_results = {
            'user_callsign':    'KD5LPB',
            'user_grid':        'EM03',
            'default_rst_bool': 'True',
            'default_rst_value':    '599',
            'default_power_bool':   'True',
            'default_power_value':  '100'
        }

        returned_results = read_config(test_file)

        self.assertEqual(expected_results, returned_results)

    def test_create_data_directory(self):
        """Data directory is not being created."""

        create_data_directory(join(test_path, 'test_data_directory'))

        self.assertTrue(os.path.exists(join(test_path, 'test_data_directory')))

    def test_database_initializer(self):
        """Database is not initialized correctly"""
        columns = {}

        db_path = join(test_path, 'miniLogbookTestDb.db')
        database_initializer(db_path)

        test_engine = create_engine('sqlite:///{0}'.format(db_path))  # Set up the test connection
        inst = inspect(test_engine)

        for table_name in inst.get_table_names():
            for column in inst.get_columns(table_name):
                columns[column.get('name')] = str(column.get('type'))

        self.assertEqual(columns.get('time'), 'DATETIME')
        self.assertEqual(columns.get('userPower'), 'FLOAT')
        self.assertEqual(columns.get('theirCall'), 'VARCHAR')
        self.assertEqual(columns.get('pkey'), 'INTEGER')
        self.assertEqual(columns.get('myLoc'), 'VARCHAR')
        self.assertEqual(columns.get('qso_uuid'), 'VARCHAR')
        self.assertEqual(columns.get('rcvdRST'), 'INTEGER')
        self.assertEqual(columns.get('freq'), 'FLOAT')
        self.assertEqual(columns.get('userCall'), 'VARCHAR')
        self.assertEqual(columns.get('sntRST'), 'INTEGER')

    def testDataPathSetter(self):
        """
        set_data_path not returning correct path.
        :return:
        """

        function_output = set_data_path()
        expected_output = join(expanduser('~'), '/var/lib/jenkins/.miniLogbook/')

        self.assertEqual(function_output, expected_output)

