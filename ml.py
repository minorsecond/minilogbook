"""
Main miniLogbook code
Robert Ross Wardrup
8/2016
"""
import sys
from os.path import expanduser, join
import os.path
from platform import system
from sqlalchemy import Column, Integer, String, Float, create_engine, Table, MetaData
from sqlalchemy.types import DateTime
import configobj
import lib.miGui as GUI
import lib.initialConfiguration as initialConfiguration
import lib.settingsWindow as settingsWindow

# Disable PyLints thinking that variables are constants
#pylint: disable=C0103

operating_system = system()


def set_data_path():
    """
    Sets the data file path and how to hide files, depending on the OS
    :return:
    """

    path = None

    user_home = expanduser('~')  # Get user home directory

    if operating_system == 'Windows':
        path = join(user_home, 'AppData/miniLogbook/')
    elif operating_system == 'Linux':
        path = join(user_home, '.miniLogbook/')
    elif operating_system == 'Darwin':
        path = join(user_home, 'Library/Application Support/miniLogbook/')

    return path


class MiniLogbook(GUI.QtGui.QMainWindow, GUI.Ui_MainWindow):
    """
    Contains the main window code
    """

    def __init__(self, test_mode):  # pragma: no cover
        """
        Initialize the GUI stuff
        :param test_mode: Disables GUI bits for unit tests.
        """

        super(MiniLogbook, self).__init__()

        if not test_mode:
            GUI.QtGui.QMainWindow.__init__(self)
            GUI.Ui_MainWindow.__init__(self)
            self.setupUi(self)

            # File menu actions
            self.settingsWindowButton.triggered.connect(self.handle_settings_window)
            self.settings_window = None
            self.recentQsos.setReadOnly(True)

    def handle_settings_window(self):  # pragma: no cover
        """
        Opens the Create DB dialog window
        :return: GUI window
        """

        if self.settings_window is None:
            self.settings_window = SettingsWindowClass(self)
        self.settings_window.show()


class SettingsWindowClass(GUI.QtGui.QDialog, settingsWindow.Ui_Dialog):
    """
    Opens the createDB dialog Window
    """

    def __init__(self, parent=None):  # pragma: no cover

        super(SettingsWindowClass, self).__init__()

        GUI.QtGui.QDialog.__init__(self, parent)
        self.setupUi(self)

        # Get the current settings, in dictionary form
        self.current_settings = read_config(settings_path)

        # Convert the rst and power booleans from string back to Booleans
        default_power_bool = bool(self.current_settings.get('default_power_bool'))
        default_rst_bool = bool(self.current_settings.get('default_rst_bool'))

        # Set default path for database file
        self.default_db_file_path = join(data_path + 'miniLogbookData.db')
        self.dbLocationLineEdit.setText(self.default_db_file_path)

        # Call the createDb_click method when the button is clicked
        self.createDbButton.clicked.connect(self.settings_window_create_db_click)
        self.SettingsWindowButtonBox.accepted.connect(self.save_settings)
        self.SettingsWindowButtonBox.rejected.connect(self.close)

        # Populate the user settings text with current settings
        self.SettingsWindowCallsign.setText(self.current_settings.get('user_callsign'))
        self.SettingsWindowGrid.setText(self.current_settings.get('user_grid'))
        self.SettingsWindowRstBool.setChecked(default_rst_bool)
        self.SettingsWindowRstValue.setText(self.current_settings.get('default_rst_value'))
        self.SettingsWindowPowerBool.setChecked(default_power_bool)
        self.SettingsWindowPowrValue.setText(self.current_settings.get('default_power_value'))

    def settings_window_create_db_click(self):  # pragma: no cover
        """
        Hnadles clicking of the "Create DB" button
        :return: a sqlite database file
        """

        db_file_path = self.dbLocationLineEdit.text()

        if len(self.dbLocationLineEdit.text()) == 0:
            print("WARNING: User has not entered any text into the dbLocationLineEdit widget.")

        if db_file_path:
            database_initializer(db_file_path)

        return db_file_path

    def save_settings(self):
        """
        Saves the settings entered in a config window
        :return:
        """

        # Pack up user settings from GUI
        user_settings = {
            'user_callsign': self.SettingsWindowCallsign.text().upper(),
            'user_grid': self.SettingsWindowGrid.text().upper(),
            'default_rst_bool': self.SettingsWindowRstBool.isChecked(),
            'default_rst_value': int(self.SettingsWindowRstValue.text()),
            'default_power_bool': self.SettingsWindowPowerBool.isChecked(),
            'default_power_value': int(self.SettingsWindowPowrValue.text())
        }

        write_config(settings_path, user_settings)


class InitialConfigurationWindow(GUI.QtGui.QDialog, initialConfiguration.Ui_Dialog):
    """
    Opens the initial config window
    """

    def __init__(self, parent=None):  # pragma: no cover
        super(InitialConfigurationWindow, self).__init__()
        GUI.QtGui.QDialog.__init__(self, parent)
        self.setupUi(self)

        # Set default path for database file
        self.default_db_file_path = join(data_path + 'miniLogbookData.db')
        self.initConfigDbLocation.setText(self.default_db_file_path)

        # Handle the buttons
        self.initConfigCreateDbButton.clicked.connect(self.init_settings_db_click)
        self.InitConfigButtonBox.accepted.connect(self.save_settings)
        self.InitConfigButtonBox.rejected.connect(sys.exit)

    def init_settings_db_click(self):  # pragma: no cover
        """
        Hnadles clicking of the "Create DB" button
        :return: a sqlite database file
        """

        db_file_path = self.initConfigDbLocation.text()

        if len(self.initConfigDbLocation.text()) == 0:
            print("WARNING: User has not entered any text into the dbLocationLineEdit widget.")

        if db_file_path:
            database_initializer(db_file_path)

        return db_file_path

    def save_settings(self):
        """
        Saves the settings entered in a config window
        :return:
        """

        # Pack up user settings from GUI
        user_settings = {
            'settings_path':    settings_path,  # This value should never change
            'user_callsign':    self.InitConfigCallsign.text().upper(),
            'user_grid':        self.InitConfigGrid.text().upper(),
            'default_rst_bool': self.InitConfigRstBool.isChecked(),
            'default_rst_value':    self.InitConfigRstValue.text(),
            'default_power_bool':   self.InitConfigPowerBool.isChecked(),
            'default_power_value':  self.InitConfigPowerValue.text()
        }

        write_config(data_path, user_settings)


def initial_config_runner(path):  # pragma: no cover
    """
    Opens the main window if there is a config file found. Else stay at the initial config page
    :return:
    """

    while not os.path.isfile(settings_path):
        open_initial_config_window(path)


def open_initial_config_window(path):  # pragma: no cover
    """
    Opens the initial configuration window
    :return:
    """
    initial_config_window = None

    if not os.path.isfile(join(path, 'miniLogbookConfig.txt')):
        print("INFO: Opening initial configuration window.")

        if not initial_config_window:
            initial_config_window = InitialConfigurationWindow()

        initial_config_window.exec_()


def create_data_directory(path):
    """
    Checks to see if directory exists and creates it if it doesn't.
    :return: a directory at dataPath
    """

    if not path:
        set_data_path()

    if not os.path.exists(path):
        os.makedirs(path)
        print("INFO: Created data directory at {0}".format(path))


def database_initializer(db_path):
    """
    Sets up the database file if it doesn't already exist
    :param db_path: Path to use store sqlite file
    :return: a sqlite file with given path
    """

    # Initialize the DB
    engine = create_engine('sqlite:///{0}'.format(db_path))
    metadata = MetaData(bind=engine)

    qso_table = Table(  #pylint: disable=W0612
        'QSOs', metadata,
        Column('pkey', Integer, primary_key=True),
        Column('qso_uuid', String),
        Column('userCall', String),
        Column('userPower', Float),
        Column('freq', Float),
        Column('time', DateTime),
        Column('myLoc', String),
        Column('theirCall', String),
        Column('rcvdRST', Integer),
        Column('sntRST', Integer)
    )

    metadata.create_all()


def write_config(file_path, settings):
    """
    Write the configuration file
    :param file_path: Path to settings file
    :param settings: dictionary containing settings to write
    :return:
    """

    config = configobj.ConfigObj()  # Initialize the config

    config.filename = file_path
    config['Callsign'] = settings.get('user_callsign')
    config['Grid'] = settings.get('user_grid')
    config['Default RST Bool'] = settings.get('default_rst_bool')
    config['Default RST to Send'] = settings.get('default_rst_value')
    config['Default Power Bool'] = settings.get('default_power_bool')
    config['Default Power to Send'] = settings.get('default_power_value')

    config.write()


def read_config(path):
    """
    Reads the configuration file
    :param path: Path to config file
    :return: a dict containing settings
    """

    settings_file = configobj.ConfigObj(r'{}'.format(path))  # Open the config file

    # Create the settings dictionary
    settings = {
        'user_callsign': settings_file['Callsign'],
        'user_grid': settings_file['Grid'],
        'default_rst_bool': settings_file['Default RST Bool'],
        'default_rst_value': settings_file['Default RST to Send'],
        'default_power_bool': settings_file['Default Power Bool'],
        'default_power_value': settings_file['Default Power to Send']
    }

    return settings


if __name__ == "__main__":  # pragma: no cover
    # Do some last-minute things that should always be done, even if called from another module
    data_path = set_data_path()  # set the path
    settings_path = join(data_path, 'miniLogbook.ini')  # Where to store the settings file

    # Initialize the GUI
    app = GUI.QtGui.QApplication(sys.argv)
    window = MiniLogbook(test_mode=False)
    create_data_directory(data_path)  # Create the data path
    initial_config_runner(data_path)
    window.show()
    sys.exit(app.exec_())
