# -*- mode: python -*-

block_cipher = None


a = Analysis(['ml.py'],
             pathex=['/Users/rwardrup/DEV/miniLogbook/bin'],
             binaries=None,
             datas=None,
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='ml',
          debug=True,
          strip=False,
          upx=True,
          console=True , icon='./bin/miniLogbook.ico')
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name='ml')