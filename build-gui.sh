#!/usr/bin/env bash
pyuic4 ./bin/miGui.ui -o ./lib/miGui.py
pyuic4 ./bin/settingsWindow.ui -o ./lib/settingsWindow.py
pyuic4 ./bin/exportDialog.ui -o ./lib/exportDialog.py
pyuic4 ./bin/initialConfiguration.ui -o ./lib/initialConfiguration.py

# Compile resource file
pyrcc4 -py3 ./bin/gui_rsc.qrc -o ./gui_rsc_rc.py